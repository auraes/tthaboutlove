Version 2.5  
Lionel Ange, 2019-2020.  
Licence Creative Commons BY-NC-ND 4.0 (CC BY-NC-ND 4.0 FR)  
https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr

**Talk to Him about Love** est un jeu d’aventure textuel en langue anglaise, avec graphismes, réalisé à l’occasion de l’Adventuron CaveJam de septembre 2019.  
Il a été développé avec le logiciel Adventuron, de Chris Ainsley.

Le jeu est jouable dans un navigateur, sur desktop ou mobile, à l’adresse suivante :  
https://auraes.itch.io/talk-to-him-about-love

L’Adventuron CaveJam :  
https://itch.io/jam/cavejam

Le logiciel Adventuron :  
https://adventuron.itch.io/